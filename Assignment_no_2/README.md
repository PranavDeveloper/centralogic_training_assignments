##  Assignment No: 2 . 
## 1. The app should have a home page displaying a list of products.
## 2. Each product should have a name, price, image
## 3. When a ListTile is clicked, navigate to a new page that displays the name and image of the clicked item.
## 4. Also add a back button to the second screen

# OutPut:-

![](./listview.gif)

## Screenshots
![](./asi2_1.png)
![](./assi2_2.png)
![](./assi2_3.png)
