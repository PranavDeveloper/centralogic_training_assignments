import 'package:assignment2/productdetails.dart';
import 'package:flutter/material.dart';


class HomePage extends StatelessWidget {
  final List<Product> products = [
    Product(name: 'Cammera', price: '₹15,000', image: 'assets/images/camera.jpeg', description:"High-resolution sensors (e.g., 24MP, 48MP) capture detailed images, with larger sensors (e.g., full-frame, APS-C) improving low-light performance and depth of field."),
    Product(name: 'Laptop', price: '₹80,000', image: 'assets/images/laptop.jpg',description: "Handles rendering images, video, and animations. Essential for gaming, video editing, and graphic design. Options include integrated GPUs and discrete GPUs like NVIDIA GeForce or AMD Radeon."),  
    Product(name: 'Mobile', price: '₹12,000', image: 'assets/images/mobile.jpeg', description: "High-resolution screens with OLED or AMOLED technology for vibrant colors and deep blacks, Typically ranges from 5 to 7 inches, offering a balance between usability and portability."),
    Product(name: 'Headphone', price: '₹5,000', image: 'assets/images/headphone.jpeg',description: "Superior audio performance with crisp highs, clear mids, and deep bass,Advanced active noise-cancelling technology to block out ambient sounds."),  
    Product(name: 'Smartwatch', price: '₹3,000', image: 'assets/images/smartwatch.png', description: "Easy navigation and interaction with apps and notifications,Continuous tracking of your heart rate for health and fitness insights,Accurate tracking of outdoor activities like running, cycling, and hiking."),
    Product(name: 'TV', price: '₹30,000', image: 'assets/images/tvimage.jpeg', description: "Modern televisions often come with 4K or even 8K resolution, providing ultra-clear and detailed images,These TVs can connect to the internet, allowing access to streaming services, social media, and web browsing."),
  ];


  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Home Page'),
      ),
      body: ListView.builder(
        itemCount: products.length + 1, 
        itemBuilder: (context, index) {
          if (index == 0) {
            return const Center(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: Text(
                  'Choose a Product',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
            );
          } else {
            final productIndex = index - 1; 
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProductDetailPage(product: products[productIndex]),
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  color: Colors.blue.withOpacity(0.1),
                ),
                child: ListTile(
                  title: Center(
                    child: Text(
                      products[productIndex].name,
                      style: const TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}