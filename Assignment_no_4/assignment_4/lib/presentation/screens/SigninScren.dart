import 'package:flutter/material.dart';
import 'package:sapdos_app/core/constants/colors.dart';

class SigninScreen extends StatelessWidget {
  const SigninScreen({super.key});
  @override
  Widget build(BuildContext context) {
    var mediaquery = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: mediaquery.size.width * 0.5,
            decoration: const BoxDecoration(
                color: AppColors.backgroundColor,
                image: DecorationImage(
                    image: AssetImage(
                      'assets/images/doctor.png',
                    ),
                    fit: BoxFit.contain)),
          ),
          Container(
            width: mediaquery.size.width * 0.5,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0), 
                bottomLeft:
                    Radius.circular(20.0), 
              ),
              color: Colors.white,
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: mediaquery.size.height * 0.1,
                  ),
                  const Text(
                            'SAPDOS',
                            style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF13235A),
                            ),
                          ),
                  SizedBox(
                    height: mediaquery.size.height * 0.14,
                  ),
                  const Text(
                            'Login to your sapdos account or\ncreate one now.', 
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xFF132235A), 
                              fontSize: 20,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                  const SizedBox(height: 50),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFF13235A), 
                              minimumSize: const Size(300, 50),
                              shape: const RoundedRectangleBorder(borderRadius:BorderRadius.all(Radius.circular(5))), // Add rounded shape
                            ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/login');
                    },
                     child: const Text(
                              'LOGIN',
                              style: TextStyle(color: Colors.white), 
                            ),
                  ),
                  const SizedBox(height: 35),
                  ElevatedButton(
                    style: OutlinedButton.styleFrom(
                              side: const BorderSide(color: Color(0xFF0A3D91)), 
                              minimumSize: const Size(300, 50),
                              shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5)), 
                            ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/signup');
                    },
                    child: const Text('SIGN-UP',
                            style: TextStyle(
                              color: Color(0xFF132235A),
                            
                              fontWeight:FontWeight.bold

                            ),),
                  ),
                  SizedBox(
                    height: mediaquery.size.height * 0.09,
                  ),
                  TextButton(
                            onPressed: () {},
                            child: const Text(
                              'Proceed as a Guest',
                              style: TextStyle(
                                
                                color: Color.fromARGB(209, 0, 0, 0),
                                decoration: TextDecoration.underline, 
                                fontSize: 20
                              ),
                            ),
                          ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
