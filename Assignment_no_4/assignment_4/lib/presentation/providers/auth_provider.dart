// lib/presentation/providers/auth_provider.dart
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'doctor_provider.dart';

class AuthProvider with ChangeNotifier {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  bool _rememberMe = false;

  bool get rememberMe => _rememberMe;
  bool _isPasswordVisible = false;

  bool get isPasswordVisible => _isPasswordVisible;

  void togglePasswordVisibility() {
    _isPasswordVisible = !_isPasswordVisible;
    notifyListeners();
  }

  void setRememberMe(bool? value) {
    _rememberMe = value ?? false;
    notifyListeners();
  }

  String? getName() {
    for (int i = 0; i < _users.length; i++) {
      if (_users[i]['email'] == emailController.text.toString()) {
        return _users[i]['name'];
      }
    }
    return "User";
  }

  final List<Map<String, String>> _users = [
    {
      "email": "pranavbochare62@gmail.com",
      "name": "Pranav Bochare",
      "phoneNumber": "7218563877",
      "password": "Pranav@123"
    },
    {
      "email": "pranavbochare66@gmail.com",
      "name": "Pranav Bochare",
      "phoneNumber": "7876665555",
      "password": "Pass@123"
    },
    {
      "email": "pavan@gmail.com",
      "name": "Pranav Bochare",
      "phoneNumber": "8976547654",
      "password": "Pass@1234"
    },
    {
      "email": "flower54@gmail.com",
      "name": "Pranav Bochare",
      "phoneNumber": "7686866667",
      "password": "Pass@12345"
    },
    {
      "email": "fruit54@gmail.com",
      "name": "Pranav Bochare",
      "phoneNumber": "8798765436",
      "password": "Pass@123456"
    }
  ];
  void login(BuildContext context) {
    final email = emailController.text;
    final password = passwordController.text;

    if (email.isEmpty || password.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Please enter both email and password")),
      );
      return;
    }

    final user = _users.firstWhere(
      (user) =>
          (user['email'] == email || user['phoneNumber'] == email) &&
          user['password'] == password,
      orElse: () => {"email": "", "phoneNumber": "", "password": ""},
    );

    if (user["email"]!.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Invalid credentials")),
      );
    } else {
      // Set the doctor's name in the DoctorProvider
      Provider.of<DoctorProvider>(context, listen: false)
          .setDoctorName(user["email"]!);
      Navigator.pushReplacementNamed(context, '/doctor');
    }
  }

  void signup(BuildContext context) {
    // Handle signup
    Navigator.pushReplacementNamed(context, '/doctor');
  }
}
