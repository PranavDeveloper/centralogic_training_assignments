import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sapdos_app/presentation/screens/SigninScren.dart';
import 'presentation/providers/auth_provider.dart';
import 'presentation/providers/doctor_provider.dart';
import 'presentation/screens/login_screen.dart';
import 'presentation/screens/registration_screen.dart';
import 'presentation/screens/doctor_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => DoctorProvider()),
      ],
      child: MaterialApp(
        title: 'Sapdos',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => SigninScreen(),
          '/login': (context) => LoginScreen(),
          '/signup': (context) => RegistrationScreen(),
          '/doctor': (context) => DoctorScreen(),
        },
      ),
    );
  }
}
