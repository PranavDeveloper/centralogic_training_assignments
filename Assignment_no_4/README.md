##  Assignment No: 4
## Develop a Flutter application that includes specific screens from the SAPDOS app as outlined below. The screens to be implemented are Doctor Screen - 01 and Login Screens - 01, 02, and 03.


### Requirements:
### Doctor Screen - 01:
#### Greeting: "Hello! Dr. [Doctor's Name]"
#### Categories: Appointment, Chat, Settings, Logout
### Sections: Pending Appointments, Completed Appointments
### Each appointment should display:
### Time
### Patient Name
### Patient Age
### Login Screens:Screens 01:
### Text: "Login to your sapdos account or create one now."
### Buttons: Login, Sign-up, Proceed as a Guest
### Screens 02:
### Text: "Welcome Back"
### Fields: Email address/Phone No., Password
### Options: Remember me, Forgot Password?
### Button: Login
### Link: "Not on Sapdos? Sign-up"
### Screens 03:
### Text: "Register"
### Fields: Email address/Phone No., Password, Confirm Password
### Button: Sign-up
### Link: "Already on Sapdos? Sign-in"
### Project Structure:
### Organize your project files logically, separating widgets, models, and screens.
### Use proper naming conventions for files and classes.
### State Management:
### Implement state management using Provider or Bloc to manage the state of the screens.


# OutPut:-

# Create Application Name 'SAPDOS'

## Screenshots
![](./assi4_1.png)
![](./assi4_2.png)
![](./assi4_3.png)
![](./assi4_4.png)

