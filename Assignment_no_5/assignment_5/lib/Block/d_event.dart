import 'package:equatable/equatable.dart';

abstract class ProviderEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadProviders extends ProviderEvent {}
