import 'package:bloc/bloc.dart';
import 'd_event.dart';
import 'd_state.dart';
import '../Repository/repositories.dart';
import '../models/doctorinfo.dart';

class ProviderBloc extends Bloc<ProviderEvent, ProviderState> {
  final ProviderRepository repository;

  ProviderBloc(this.repository) : super(ProvidersLoading()) {
    on<LoadProviders>((event, emit) async {
      try {
        final ProviderResponseData providerResponseData = await repository.fetchProviders();
        emit(ProvidersLoaded(providerResponseData));
      } catch (_) {
        emit(ProvidersError());
      }
    });
  }
}
