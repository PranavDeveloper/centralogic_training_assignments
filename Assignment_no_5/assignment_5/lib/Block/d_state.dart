// bloc/provider_state.dart
import 'package:equatable/equatable.dart';
import '../models/doctorinfo.dart';

abstract class ProviderState extends Equatable {
  @override
  List<Object> get props => [];
}

class ProvidersLoading extends ProviderState {}

class ProvidersLoaded extends ProviderState {
  final ProviderResponseData providerResponseData;

  ProvidersLoaded(this.providerResponseData);

  @override
  List<Object> get props => [providerResponseData];
}

class ProvidersError extends ProviderState {}
