import 'package:assignment_5/Block/d_block.dart';
import 'package:assignment_5/Block/d_event.dart';
import 'package:assignment_5/Block/d_state.dart';
import 'package:assignment_5/widget/cards.dart';
import 'package:assignment_5/widget/resposnsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../models/doctorinfo.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        backgroundColor: const Color.fromRGBO(19, 35, 90, 1),
        child: ListView(
          padding: EdgeInsets.zero,
          children: const [
            DrawerHeader(
              decoration: BoxDecoration(),
              child: Text(
                'SAPDOS',
                style: TextStyle(color: Colors.white, fontSize: 24),
              ),
            ),
            ListTile(
              leading: Icon(Icons.category, color: Colors.white),
              title: Text(
                'Categories',
                style: TextStyle(color: Colors.white),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.calendar_today,
                color: Colors.white,
              ),
              title: Text('Appointment', style: TextStyle(color: Colors.white)),
            ),
            ListTile(
              leading: Icon(Icons.chat, color: Colors.white),
              title: Text('Chat', style: TextStyle(color: Colors.white)),
            ),
            ListTile(
              leading: Icon(Icons.settings, color: Colors.white),
              title: Text('Settings', style: TextStyle(color: Colors.white)),
            ),
            ListTile(
              leading: Icon(Icons.logout, color: Colors.white),
              title: Text('Logout', style: TextStyle(color: Colors.white)),
            )
          ],
        ),
      ),
      appBar: AppBar(title: const Text('SAPDOS')),
      body: BlocProvider(
        create: (context) => ProviderBloc(context.read())..add(LoadProviders()),
        child: BlocBuilder<ProviderBloc, ProviderState>(
          builder: (context, state) {
            if (state is ProvidersLoading) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is ProvidersLoaded) {
              final responseData = state.providerResponseData;
              return Column(
                children: [
                  UserGreeting(user: responseData.userProfile),
                  const Padding(
                    padding: EdgeInsets.only(left: 400,right:30),
                  
                    child: MaterialBanner(
                      content: Text(
                        'Doctor List',
                        style: TextStyle(color: Colors.white),
                      ),
                      backgroundColor: Color.fromRGBO(19, 35, 90, 1),
                      actions: <Widget>[
                        Icon(
                          Icons.filter_list,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ResponsiveLayout(
                      mobileLayout: ProviderList(providers: responseData.providersList),
                      desktopLayout: ProviderGrid(providers: responseData.providersList),
                    ),
                  ),
                ],
              );
            } else if (state is ProvidersError) {
              return const Center(child: Text('Failed to fetch healthcare providers'));
            }
            return Container(

            );
          },
        ),
      ),
    );
  }
}

class UserGreeting extends StatelessWidget {
  final UserProfile   user;

  const UserGreeting({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:400,top:16,right:50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
           Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                user.greetingMessage,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              Text(
                user.fullName,
                style: TextStyle(fontSize: 20),
              )
            ],
          ),
          const SizedBox(width: 16),
        CircleAvatar(
            backgroundImage: NetworkImage(user.profilePicture),
            radius: 30,
          ),
          
        ],
      ),
    );
  }
}

class ProviderList extends StatelessWidget {
  final List providers;

  const ProviderList({super.key, required this.providers});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: providers.length,
      itemBuilder: (context, index) {
        return ProviderCard(provider: providers[index]);
      },
    );
  }
}

class ProviderGrid extends StatelessWidget {
  final List providers;

  const ProviderGrid({super.key, required this.providers});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:400,right: 30),
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 0.4,
        ),
        itemCount: providers.length,
        itemBuilder: (context, index) {
          return ProviderCard(provider: providers[index]);
        },
      ),
    );
  }
}
