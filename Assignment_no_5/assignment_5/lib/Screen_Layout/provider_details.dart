import 'package:assignment_5/models/doctorinfo.dart';
import 'package:flutter/material.dart';


class ProviderDetailScreen extends StatelessWidget {
  final HealthcareProvider provider;

  const ProviderDetailScreen({super.key, required this.provider});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildProviderInfo(),
              const SizedBox(height: 25),
              _buildAvailableSlots(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildProviderInfo() {
    return Padding(
      padding: const EdgeInsets.all(80.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
              color: Color(0xff7E91D4),
              borderRadius: BorderRadius.all(Radius.circular(5))
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 50,right:50,top:20,bottom:20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: Image.network(
                  provider.imageUrl,
                  width: 250,
                  height: 300,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      provider.name,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Text("      "),
                    Row(
                      children: List.generate(5, (index) {
                        return Icon(
                          Icons.star,
                          size: 16,
                          color: index < 3 ? Colors.amber : Colors.grey,
                        );
                      }),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    const Icon(Icons.medical_services, size: 16),
                    const SizedBox(width: 4),
                    Text(provider.specialty),
                    const Text("      "),
                    const Icon(Icons.school, size: 16),
                    const SizedBox(width: 4),
                    const Text('5 Years'),
                  ],
                ),
                const SizedBox(height: 30),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Description",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(19, 35, 90, 1),
                      ),
                    ),
                    Text(
                        "Dr. ${provider.name},dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolore eu fugiat nullapariatur. ")
                  ],
                ),
                const SizedBox(height: 8),
              
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAvailableSlots(BuildContext context) {
    List<String> timeSlots = [
      '10:00 - 10:15 AM',
      '11:00 - 11:15 AM',
      '12:00 - 12:15 PM',
      '01:00 - 01:15 PM',
      '02:00 - 02:15 PM',
      '03:00 - 03:15 PM',
    ];

    String? selectedTimeSlot;

    return Padding(
      padding: const EdgeInsets.only(left:80),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const MaterialBanner(
            content: Text(
              'Available slots',
              style: TextStyle(color: Colors.white),
            ),
            leading: Icon(
              Icons.timer,
              color: Colors.white,
            ),
            backgroundColor: Color.fromRGBO(19, 35, 90, 1),
            actions: <Widget>[
              Icon(
                Icons.calendar_month,
                color: Colors.white,
              ),
            ],
          ),
          const SizedBox(height: 8),
          StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return GridView.count(
                shrinkWrap: true,
                crossAxisCount: 3,
                crossAxisSpacing: 1,
                mainAxisSpacing: 2,
                childAspectRatio: 8,
                children: List.generate(timeSlots.length, (index) {
                  return Card(
                    color: const Color(0xffDCE0ED),
                    child: RadioListTile<String>(
                      value: timeSlots[index],
                      groupValue: selectedTimeSlot,
                      title: Text(timeSlots[index]),
                      onChanged: (value) {
                        setState(() {
                          selectedTimeSlot = value;
                        });
                      },
                      activeColor: Colors.blueGrey,
                    ),
                  );
                }),
              );
            },
          ),
          const SizedBox(height: 16),
          Center(
            child: ElevatedButton(
              onPressed: () {
                
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: const Color.fromRGBO(19, 35, 90, 1),
                foregroundColor: Colors.white,
                padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 12),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              child: const Text('Book Appointment'),
            ),
          ),
        ],
      ),
    );
  }
}
