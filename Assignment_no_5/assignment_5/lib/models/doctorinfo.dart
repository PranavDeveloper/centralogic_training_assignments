class UserProfile {
  final String greetingMessage;
  final String fullName;
  final String profilePicture;

  UserProfile({required this.greetingMessage, required this.fullName, required this.profilePicture});

  factory UserProfile.fromJson(Map<String, dynamic> json) {
    return UserProfile(
      greetingMessage: json['greetingMessage'],
      fullName: json['fullName'],
      profilePicture: json['profilePicture'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'greetingMessage': greetingMessage,
      'fullName': fullName,
      'profilePicture': profilePicture,
    };
  }
}

class HealthcareProvider {
  final String imageUrl;
  final String name;
  final String specialty;
  final String iconPath;
  final int consultationFee;

  HealthcareProvider({
    required this.imageUrl,
    required this.name,
    required this.specialty,
    required this.iconPath,
    required this.consultationFee,
  });

  factory HealthcareProvider.fromJson(Map<String, dynamic> json) {
    return HealthcareProvider(
      imageUrl: json['imageUrl'],
      name: json['name'],
      specialty: json['specialty'],
      iconPath: json['iconPath'],
      consultationFee: json['consultationFee'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'imageUrl': imageUrl,
      'name': name,
      'specialty': specialty,
      'iconPath': iconPath,
      'consultationFee': consultationFee,
    };
  }
}

class ProviderResponseData {
  final UserProfile userProfile;
  final List<HealthcareProvider> providersList;

  ProviderResponseData({required this.userProfile, required this.providersList});

  factory ProviderResponseData.fromJson(Map<String, dynamic> json) {
    var providersListFromJson = json['providersList'] as List;
    List<HealthcareProvider> providersList =
        providersListFromJson.map((i) => HealthcareProvider.fromJson(i)).toList();

    return ProviderResponseData(
      userProfile: UserProfile.fromJson(json['userProfile']),
      providersList: providersList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'userProfile': userProfile.toJson(),
      'providersList': providersList.map((provider) => provider.toJson()).toList(),
    };
  }
}
