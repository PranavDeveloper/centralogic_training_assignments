import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import '../models/doctorinfo.dart';

class ProviderRepository {
  Future<ProviderResponseData> fetchProviders() async {
    try {
      final jsonString =
          await rootBundle.loadString('assets/JsonFolder/list_Json.json');

      final jsonResponse = json.decode(jsonString);
      final providerResponseData = ProviderResponseData.fromJson(jsonResponse);

      return providerResponseData;
    } catch (e) {
      throw Exception('Failed to load providers');
    }
  }
}
