##  Assignment No: 5
## Implement JSON & Serialization in the attached UI using Flutter clean architecture .

### Requirements:
### Ensure the code adheres to clean architecture practices, including proper separation of concerns, dependency injection, and use of repositories.
### Build the page according to the Figma UI Design.
### Create a responsive page for Desktop view.
### Create Patient screens using JSON data binding:
### (Create JSON data referring to the attached JSON of Screen-01)
# OutPut:-

# Create Application Name 'SAPDOS'

## Screenshots
![](./assi5_1.png)
![](./assi5_2.png)

