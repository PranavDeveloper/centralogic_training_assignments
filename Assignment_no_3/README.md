##  Assignment No: 3 
## Add to Cart Functionality with Flutter Provider
## Create a Flutter application with an "Add to Cart" functionality using the Provider package. Users will be ## able to add items to a cart, and view all added items on a cart page.

### Requirements:
### Main Page:
### Display a list of products with an "Add to Cart" button for each product
### Implement "Add to Cart" functionality for each product using the Provider
### Cart Page:
### Display all items added to the cart.
### Show the total price of all items in the cart.
### Provide an option to remove items from the cart.

# OutPut:-

# Create Application Name 'Shopmate'

## Screenshots
![](./assi3_1.png)
![](./assi3_2.png)
![](./assi3_3.png)
