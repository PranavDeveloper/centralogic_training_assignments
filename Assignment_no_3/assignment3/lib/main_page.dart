import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart'; 
import 'cart.dart';
import 'product.dart';

class MainPage extends StatelessWidget {
  final List<Product> products = [
    Product(id: '1', title: 'Laptop', price: 40000, imagePath: 'assets/images/laptop.jpg'),
    Product(id: '2', title: 'Camera', price: 20000, imagePath: 'assets/images/camera.jpeg'),
    Product(id: '3', title: 'Fan', price: 2000, imagePath: 'assets/images/fan.jpg'),
    Product(id: '4', title: 'Hairdryer', price: 5000, imagePath: 'assets/images/hairdryer.jpg'),
    Product(id: '5', title: 'Headphone', price: 1500, imagePath: 'assets/images/headphone.jpeg'),
    Product(id: '6', title: 'Iron', price: 1400, imagePath: 'assets/images/iron.jpg'),
    Product(id: '7', title: 'Microwave', price: 10000, imagePath: 'assets/images/microwave.jpeg'),
    Product(id: '8', title: 'Smartwatch', price: 1200, imagePath: 'assets/images/smartwatch.png'),
    Product(id: '9', title: 'TV', price: 1300, imagePath: 'assets/images/tv.jpeg'),
    Product(id: '10', title: 'USB', price: 700, imagePath: 'assets/images/usb.jpg'),
    Product(id: '11', title: 'Mobile', price: 25000, imagePath: 'assets/images/mobile.jpeg'),
  ];

  MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Our Product List',
          style: GoogleFonts.poppins(
            fontSize: 20, 
            fontWeight: FontWeight.w600,
            color: Colors.black, 
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.shopping_cart,
              size: 40,
            ), 
            onPressed: () {
              Navigator.pushNamed(context, '/cart');
            },
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: products.length,
        itemBuilder: (ctx, i) => Container(
          margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: const Color.fromRGBO(255, 255, 255, 1),
            borderRadius: BorderRadius.circular(14),
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.15),
                offset: Offset(0, 3),
                blurRadius: 10,
              ),
            ],
          ),
          child: ListTile(
            leading: SizedBox(
              width: 60, 
              height: 60, 
              child: ClipOval(
                child: Image.asset(
                  products[i].imagePath,
                  width: 60,
                  height: 60,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              products[i].title,
              style: GoogleFonts.poppins(
                fontSize: 18, 
                fontWeight: FontWeight.w500, 
                color: const Color.fromRGBO(0, 0, 0, 1),
              ),
            ),
            subtitle: Text(
              '₹${products[i].price}',
              style: GoogleFonts.poppins(), 
            ),
            trailing: IconButton(
              icon: const Icon(
                Icons.add_shopping_cart,
                size: 30,
              ), 
              onPressed: () {
                Provider.of<Cart>(context, listen: false).addItem(products[i]);
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text('${products[i].title} added to cart'),
                    duration: const Duration(seconds: 2),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
