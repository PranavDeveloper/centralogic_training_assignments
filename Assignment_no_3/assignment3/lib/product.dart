
class Product {
  final String id;
  final String title;
  final double price;
  final String imagePath;

  Product({
    required this.id,
    required this.title,
    required this.price,
    required this.imagePath,
  });
}
