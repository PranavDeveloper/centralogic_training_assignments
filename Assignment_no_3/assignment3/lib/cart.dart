
import 'package:flutter/material.dart';
import 'product.dart';

class Cart with ChangeNotifier {
  final Map<String, Product> _items = {};

  Map<String, Product> get items => _items;

  void addItem(Product product) {
    _items.putIfAbsent(product.id, () => product);
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  double get totalPrice {
    return _items.values.fold(0.0, (sum, item) => sum + item.price);
  }
}
